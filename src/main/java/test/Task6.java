package test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task6 {
    public static Logger logger6 = LogManager.getLogger(Task6.class);

    public static void main(String[] args) {

            logger6.trace("This is trace Log.");
            logger6.debug("This is debug Log.");
            logger6.info("This is info Log.");
            logger6.warn("This is warn Log.");
            logger6.error("This is error Log.");
            logger6.fatal("This is fatal Log.");
    }
}

