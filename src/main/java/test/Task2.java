package test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task2 {
    public static Logger logger2 = LogManager.getLogger(Task2.class);

    public static void main(String[] args) {
        int i = 0;
        while (i <= 5) {
            logger2.trace("This is trace Log.");
            logger2.debug("This is debug Log.");
            logger2.info("This is info Log.");
            logger2.warn("This is warn Log.");
            logger2.error("This is error Log.");
            logger2.fatal("This is fatal Log.");
            i++;
        }

    }
}

