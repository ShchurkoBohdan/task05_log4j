package test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task3 {
    public static Logger logger3 = LogManager.getLogger(Task3.class);
    public static void main(String[] args) {
        logger3.trace("This is trace Log.");
        logger3.debug("This is debug Log.");
        logger3.info("This is info Log.");
        logger3.warn("This is warn Log.");
        logger3.error("This is error Log.");
        logger3.fatal("This is fatal Log.");
    }
}
