package test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task7 {
    public static Logger logger7 = LogManager.getLogger(Task7.class);
    public static void main(String[] args) {
        logger7.trace("This is trace Log.");
        logger7.debug("This is debug Log.");
        logger7.info("This is info Log.");
        logger7.warn("This is warn Log.");
        logger7.error("This is error Log.");
        logger7.fatal("This is fatal Log.");
    }
}
