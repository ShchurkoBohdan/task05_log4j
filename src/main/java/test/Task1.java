package test;

import org.apache.logging.log4j.*;

public class Task1 {
    public static Logger logger = LogManager.getLogger(Task1.class);
    public static void main(String[] args) {

        logger.trace("This is trace Log.");
        logger.debug("This is debug Log.");
        logger.info("This is info Log.");
        logger.warn("This is warn Log.");
        logger.error("This is error Log.");
        logger.fatal("This is fatal Log.");


    }
}
